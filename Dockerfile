FROM ubuntu:xenial

RUN sed -i -e '/^# deb-src .* main/s/^# //' /etc/apt/sources.list && apt-get update

RUN apt-get build-dep -y ledger
RUN apt-get install -y git

RUN mkdir -- /build && \
    git -C /build init && \
    git -C /build remote add origin https://github.com/ledger/ledger.git

WORKDIR /build
